<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section class="hero-slider">
		<?php if(have_rows('hero_slides')): while(have_rows('hero_slides')) : the_row(); ?>
		 
		    <?php if( get_row_layout() == 'slide' ): ?>

				<div class="hero-slide">
					<div class="photo cover" style="background-image: url(<?php $image = get_sub_field('photo'); echo $image['url']; ?>);">
						<div class="content">

						</div>
					</div>   		

						
					<div class="info">
						<div class="headline">
							<h1><?php the_sub_field('headline'); ?></h1>
						</div>

						<div class="copy p2">
							<p><?php the_sub_field('copy'); ?></p>
						</div>

						<div class="cta">
							<a href="<?php the_sub_field('cta_link'); ?>" class="btn blue"><?php the_sub_field('cta_label'); ?></a>
						</div>						
					</div>	

				</div>
				
		    <?php endif; ?>
		 
		<?php endwhile; endif; ?>	
	</section>


	<section id="about" class="cover">
		<div class="wrapper">

			<div class="about-wrapper">
				<div class="info">
					<div class="icon">
						<img src="<?php $image = get_field('about_icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

					<div class="headline">
						<h2><?php the_field('about_headline'); ?></h2>
					</div>
					
					<div class="copy p2">
						<?php the_field('about_copy'); ?>
					</div>

					<div class="cta">
						<a href="<?php the_field('about_cta_link'); ?>" class="btn white"><?php the_field('about_cta_label'); ?></a>
					</div>
				</div>
				
				<div class="about-nav">
					<div class="headline">
						<h3><?php the_field('about_nav_headline'); ?></h3>
					</div>

					<div class="item-wrapper">
						<?php if(have_rows('immune_support_dropdown', 'options')): while(have_rows('immune_support_dropdown', 'options')): the_row(); ?>

							<?php $post_id = get_sub_field('page', false, false); if( $post_id ): ?>

								<div class="item">
									<div class="image">
										<a href="<?php echo get_the_permalink($post_id); ?>" class="cover" style="background-image: url(<?php $image = get_sub_field('image'); echo $image['url']; ?>);">
										</a>    		
									</div>

									<div class="link">
										<a href="<?php echo get_the_permalink($post_id); ?>"><?php echo get_the_title($post_id); ?></a>
									</div>
								</div>

							<?php endif; ?>
							
						<?php endwhile; endif; ?>		
					</div>
				</div>				
			</div>

		</div>
	</section>


	<section id="quote-slider">
		<div class="wrapper">

			<div class="quote-wrapper">
				<?php if(have_rows('quotes')): while(have_rows('quotes')): the_row(); ?>

					<div class="quote">
						<div class="copy">
							<blockquote>
								<?php the_sub_field('quote'); ?>
							</blockquote>
						</div>

						<div class="source">
							<div class="photo">
								<img src="<?php $image = get_sub_field('source_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>
							<div class="details">
								<div class="content">
									<h4><?php the_sub_field('source_name'); ?></h4>
									<h5><?php the_sub_field('source_location'); ?></h5>
								</div>
							</div>
						</div>

					</div>

				<?php endwhile; endif; ?>
			</div>
			
		</div>
	</section>


	<?php if(get_field("show_offer") == true): ?>

		<section id="offer">
			<div class="wrapper">
			
				<div class="offer-wrapper">
					
					<div class="banner">
						<h3><?php the_field('offer_banner_headline'); ?></h3>
						<h4><?php the_field('offer_banner_sub_headline'); ?></h4>
					</div>	

					<div class="photo">
						<img src="<?php $image = get_field('offer_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

					<div class="info">
						<div class="headline">
							<h1><?php the_field('offer_headline'); ?></h1>
						</div>
						
						<div class="copy p2">
							<?php the_field('offer_copy'); ?>
						</div>

						<div class="cta">
							<a href="<?php the_field('offer_cta_link'); ?>" class="btn blue"><?php the_field('offer_cta_label'); ?></a>
							<a href="<?php the_field('offer_details_link'); ?>" class="detail"><?php the_field('offer_details_link_label'); ?></a>
						</div>
					</div>

				</div>
				
			</div>
		</section>

	<?php endif; ?>


	<section id="resources">
		<div class="wrapper">
						
			<div class="photo">
				<img src="<?php $image = get_field('resources_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<div class="info">
				<div class="headline">
					<h3>Resources & Research</h3>
					<h1><?php the_field('resources_headline'); ?></h1>
				</div>
				
				<div class="copy p2">
					<?php the_field('resources_copy'); ?>
				</div>

				<div class="cta">
					<a href="<?php the_field('resources_cta_link'); ?>" class="btn blue"><?php the_field('resources_cta_label'); ?></a>

					<div class="detail">
						<a href="<?php the_field('resources_all_link'); ?>"><?php the_field('resources_all_link_label'); ?></a>				
					</div>
				</div>
			</div>

		</div>
	</section>	

<?php get_footer(); ?>