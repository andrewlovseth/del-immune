<?php if(get_field('middle_content_copy')): ?>

	<section class="middle-content inset">
		<div class="wrapper">
			
			<div class="content">

				<div class="headline">
					<h3><?php the_field('middle_content_headline'); ?></h3>
				</div>

				<div class="copy p2 extended">
					<?php the_field('middle_content_copy'); ?>	
				</div>
				
			</div>


		</div>
	</section>

<?php endif; ?>