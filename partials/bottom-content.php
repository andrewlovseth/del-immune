<?php if(get_field('bottom_content_copy')): ?>

	<section class="bottom-content inset">
		<div class="wrapper">

			<div class="content">
				<div class="copy p2 extended">
					<?php the_field('bottom_content_copy'); ?>
				</div>
			</div>

		</div>
	</section>

<?php endif; ?>