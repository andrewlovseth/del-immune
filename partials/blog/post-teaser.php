<article class="list">
	<div class="info">
		<div class="headline">
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		</div>

		<div class="cta">
			<a href="<?php the_permalink(); ?>" class="text">Read More</a>
		</div>
	</div>
</article>
