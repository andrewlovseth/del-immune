<section class="top-content inset">
	<div class="wrapper">

		<div class="content">

			<section class="page-header">
				<div class="breadcrumbs">
					
					<?php if(is_page_template('immune-support-index.php')): ?>

						<?php get_template_part('partials/breadcrumbs/level-two-page'); ?>

					<?php elseif(is_page_template('immune-support-detail.php')): ?>

						<?php get_template_part('partials/breadcrumbs/immune-support-detail'); ?>

					<?php else: ?>

						<?php get_template_part('partials/breadcrumbs/level-two-page'); ?>

					<?php endif; ?>

				</div>

				<div class="page-title headline">
					<h1>
						<?php if(get_field('top_content_headline')): ?>
							<?php the_field('top_content_headline'); ?>
						<?php else: ?>
							<?php the_title(); ?>
						<?php endif; ?>
					</h1>
				</div>
			</section>

			<div class="copy p2 extended">
				<?php the_field('top_content_copy'); ?>
			</div>

		</div>
		
	</div>
</section>