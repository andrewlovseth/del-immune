<div class="categories-dropdown">

	<?php 
		wp_dropdown_categories( array(
			'orderby' => 'name',
			'order' => 'ASC',
		    'hide_empty'   => 1,
		    'exclude' => array(1),
		    'show_option_all' => 'Show All Categories'
		) );
	?>		

	<script>
		var dropdown = document.getElementById("cat");
		function onCatChange() {
			if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
				location.href = "<?php echo esc_url( home_url( '/' ) ); ?>?cat="+dropdown.options[dropdown.selectedIndex].value;
			} else {
				location.href = "<?php echo esc_url( home_url( '/resources/' ) ); ?>";
			}
		}
		dropdown.onchange = onCatChange;

	</script>

	
</div>