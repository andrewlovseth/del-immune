<section class="hero">
	<div class="content">
		<img src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	</div>
</section>