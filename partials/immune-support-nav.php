<section class="immune-support-nav">
	<div class="wrapper">

		<div class="headline">
			<h2>
				<?php 
					$page = get_page_by_path('immune-support');
					the_field('immune_support_nav_headline', $page->ID);
				?>				
			</h2>
		</div>

		<div class="item-wrapper wrapper">
			<?php if(have_rows('immune_support_dropdown', 'options')): while(have_rows('immune_support_dropdown', 'options')): the_row(); ?>

				<?php $post_id = get_sub_field('page', false, false); if( $post_id ): ?>

					<div class="item<?php if( $post->post_name == get_post_field('post_name', $post_id) ): ?> item-active<?php endif; ?>">
						<div class="image">
							<a href="<?php echo get_the_permalink($post_id); ?>" class="cover" style="background-image: url(<?php $image = get_sub_field('image'); echo $image['url']; ?>);">
							</a>    		
						</div>

						<div class="info">
							<h4><a href="<?php echo get_the_permalink($post_id); ?>"><?php echo get_the_title($post_id); ?></a></h4>
						</div>
					</div>

				<?php endif; ?>

			<?php endwhile; endif; ?>					
		</div>

	</div>
</section>