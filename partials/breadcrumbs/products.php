<div class="breadcrumbs">
	<div class="level first-level"><a href="<?php echo site_url('/'); ?>" class="page-name">Home</a><span class="caret">&gt;</span> </div>
	<div class="level second-level"><a href="<?php echo site_url('/shop-products/'); ?>" class="page-name">Shop Products</a><span class="caret">&gt;</span> </div>
	<div class="level third-level active"><span class="page-name"><?php the_title(); ?></span></div>
</div>