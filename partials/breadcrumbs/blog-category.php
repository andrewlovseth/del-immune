<div class="breadcrumbs">
	<div class="level first-level"><a href="<?php echo site_url('/'); ?>" class="page-name">Home</a><span class="caret">&gt;</span> </div>
	<div class="level second-level"><a href="<?php echo site_url('/resources/'); ?>" class="page-name">Resources</a><span class="caret">&gt;</span> </div>
	<div class="level third-level active"><span class="page-name"><?php single_cat_title(); ?></span></div>

</div>