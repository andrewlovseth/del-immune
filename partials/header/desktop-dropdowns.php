<section id="dropdowns">
	<div class="dropdown-wrapper">

		<section id="products-dropdown" class="dropdown">
			<div class="wrapper">

				<div class="item-wrapper">

					<?php if(have_rows('shop_products_dropdown', 'options')): while(have_rows('shop_products_dropdown', 'options')): the_row(); ?>
						<?php $post_id = get_sub_field('page', false, false); if( $post_id ): ?>

							<div class="item">
								<div class="image">
									<a href="<?php echo get_the_permalink($post_id); ?>">
										<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									</a>    		
								</div>

								<div class="info">
									<h4><a href="<?php echo get_the_permalink($post_id); ?>"><?php the_sub_field('label'); ?></a></h4>
								</div>
							</div>

						<?php endif; ?>
					<?php endwhile; endif; ?>	
		
				</div>
				
			</div>

			<?php get_template_part('partials/header/pro-accounts'); ?>
		</section>


		<section id="immune-support-dropdown" class="dropdown">
			<div class="wrapper">
				
				<div class="item-wrapper">
					<?php if(have_rows('immune_support_dropdown', 'options')): while(have_rows('immune_support_dropdown', 'options')): the_row(); ?>

						<?php $post_id = get_sub_field('page', false, false); if( $post_id ): ?>

							<div class="item">
								<div class="image">
									<a href="<?php echo get_the_permalink($post_id); ?>">
										<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									</a>    		
								</div>

								<div class="info">
									<h4><a href="<?php echo get_the_permalink($post_id); ?>"><?php echo get_the_title($post_id); ?></a></h4>
								</div>
							</div>

						<?php endif; ?>

					<?php endwhile; endif; ?>					
				</div>

			</div>

			<?php get_template_part('partials/header/pro-accounts'); ?>
		</section>


		<section id="resources-dropdown" class="dropdown">
			<div class="wrapper">
				
				<div class="item-wrapper">
					<?php if(have_rows('resources_dropdown', 'options')): while(have_rows('resources_dropdown', 'options')): the_row(); ?>
						<?php get_template_part('partials/header/dropdown-item'); ?>
					<?php endwhile; endif; ?>					
				</div>

			</div>

			<?php get_template_part('partials/header/pro-accounts'); ?>
		</section>

	</div>
</section>