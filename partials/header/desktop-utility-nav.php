<div class="utility-nav desktop">
	<div class="utility-wrapper">

		<div class="search">
			<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
			    <input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search products, articles and more', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s"/>
			    <input type="submit" class="search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
			</form>		
		</div>
		
		<a href="<?php echo site_url('/account-profile/'); ?>">My Account</a>

		<?php if(is_user_logged_in()): ?>	
			<a href="<?php echo wp_logout_url(); ?>" class="logout">Logout</a>
		<?php endif; ?>
		
	</div>
</div>
