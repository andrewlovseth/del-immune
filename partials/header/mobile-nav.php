<nav class="mobile">
	<div class="wrapper">
		<div class="slide-wrapper">
			<div class="main-screen">
				<a href="#" class="subnav products" data-subnav="products-subnav"><span>Shop Products</span></a>
				<a href="#" class="subnav immune-support" data-subnav="immune-support-subnav"><span>Immune Support</span></a>
				<a href="#" class="subnav resources" data-subnav="resources-subnav"><span>Resources</span></a>
				<a href="<?php echo site_url('/about-us/'); ?>"><span>About Us</span></a>
				<a href="<?php echo site_url('/account-profile/'); ?>" class="small"><span>My Account</span></a>

				<?php if(is_user_logged_in()): ?>
					<a href="<?php echo wp_logout_url(); ?>" class="small logout">Logout</a>
				<?php endif; ?>
			</div>

			<div class="subnav-screen">

				<div id="products-subnav" class="subnav">
					<div class="back">
						<a href="#"></a>
					</div>

					<div class="header">
						<h5><a href="<?php echo site_url('/shop-products/'); ?>">Shop Products</a></h5>
					</div>

					<div class="links">
						<?php if(have_rows('shop_products_dropdown', 'options')): while(have_rows('shop_products_dropdown', 'options')): the_row(); ?>
							<a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>
						<?php endwhile; endif; ?>								
					</div>					
				</div>

				<div id="immune-support-subnav" class="subnav">
					<div class="back">
						<a href="#"></a>
					</div>

					<div class="header">
						<h5><a href="<?php echo site_url('/immune-support/'); ?>">Immune Support</a></h5>
					</div>

					<div class="links">
						<?php if(have_rows('immune_support_dropdown', 'options')): while(have_rows('immune_support_dropdown', 'options')): the_row(); ?>

							<?php $post_id = get_sub_field('page', false, false); if( $post_id ): ?>
								<a href="<?php echo get_the_permalink($post_id); ?>"><?php echo get_the_title($post_id); ?></a>
							<?php endif; ?>

						<?php endwhile; endif; ?>								
					</div>					
				</div>

				<div id="resources-subnav" class="subnav">
					<div class="back">
						<a href="#"></a>
					</div>

					<div class="header">
						<h5><a href="<?php echo site_url('/resources/'); ?>">Resources</a></h5>
					</div>

					<div class="links">
						<?php if(have_rows('resources_dropdown', 'options')): while(have_rows('resources_dropdown', 'options')): the_row(); ?>
							<a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>
						<?php endwhile; endif; ?>	
					</div>					
				</div>
				
			</div>
		</div>
	</div>

	<div class="search">
		<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
		    <input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search products, articles and more', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s"/>
		    <input type="submit" class="search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
		</form>		
	</div>
	
	<?php get_template_part('partials/header/pro-accounts'); ?>

</nav>