<nav class="desktop">	
	<a href="<?php echo site_url('/shop-products/'); ?>" class="dropdown products" data-dropdown="products-dropdown">
		<span>Shop Products</span>
	</a>

	<a href="<?php echo site_url('/immune-support/'); ?>" class="dropdown immune-support" data-dropdown="immune-support-dropdown">
		<span>Immune Support</span>
	</a>

	<a href="<?php echo site_url('/resources/'); ?>" class="dropdown resources" data-dropdown="resources-dropdown">
		<span>Resources</span>
	</a>

	<a href="<?php echo site_url('/about-us/'); ?>">
		<span>About Us</span>
	</a>	
</nav>