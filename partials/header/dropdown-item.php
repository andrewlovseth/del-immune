<div class="item">
	<div class="image">
		<a href="<?php the_sub_field('link'); ?>">
			<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</a>    		
	</div>

	<div class="info">
		<h4><a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a></h4>
	</div>
</div>