<div class="share">
	<h5>Share This</h5>

	<div class="email">
		<a href="mailto:?subject=<?php echo get_the_title(); ?>&body=<?php echo get_permalink(); ?>">
			<img src="<?php bloginfo('template_directory') ?>/images/share-email.png" alt="Email">
		</a>
	</div>

	<div class="facebook">
		<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" rel="external">
			<img src="<?php bloginfo('template_directory') ?>/images/share-facebook.png" alt="Facebook">
		</a>
	</div>

	<div class="twitter">
		<a href="https://twitter.com/intent/tweet/?text=<?php echo get_the_title(); ?>+<?php echo get_permalink(); ?>" rel="external">
			<img src="<?php bloginfo('template_directory') ?>/images/share-twitter.png" alt="Twitter">
		</a>
	</div>
</div>