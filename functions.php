<?php

/*

    ----------------------------------------------------------------------
    					XX Theme Support
    ----------------------------------------------------------------------

*/

add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ));
add_theme_support( 'title-tag' );




// Enqueue custom styles and scripts
function bearsmith_enqueue_styles_and_scripts() {
    $dir = get_template_directory_uri();

    // Add style.css and third-party css
    wp_enqueue_style( 'custom-css', $dir. '/style.css', array(), false, 'all');

    // Add plugins.js & site.js (with jQuery dependency)
    wp_enqueue_script('plugins', $dir . '/js/plugins.js', array(), false, true );
    wp_enqueue_script('scripts', $dir . '/js/site.js', array(), false, true );
}
add_action( 'wp_enqueue_scripts', 'bearsmith_enqueue_styles_and_scripts' );





/*

    ----------------------------------------------------------------------
    					XX Custom Functions
    ----------------------------------------------------------------------

*/

/*
   functions.php
   Use alm_query_args filter to pass data to SearchWP.
   https://connekthq.com/plugins/ajax-load-more/docs/filter-hooks/#alm_query_args
*/
function my_alm_query_args_searchwp($args){   
   $engine = 'default'; // default = default
   $args = apply_filters('alm_searchwp', $args, $engine); // Make call to alm_searchwp filter
   return $args;
}
add_filter( 'alm_query_args_searchwp', 'my_alm_query_args_searchwp');




/*

    ----------------------------------------------------------------------
    					XX Advanced Custom Fields
    ----------------------------------------------------------------------

*/

function my_relationship_query( $args, $field, $post_id ) {
    $args['orderby'] = 'date';
    $args['order'] = 'DESC';
    return $args;
}


if( function_exists('acf_add_options_page') ) {
    acf_add_options_page('Site Options');
}


// filter for every field
add_filter('acf/fields/relationship/query', 'my_relationship_query', 10, 3);

function my_acf_admin_head() {
	?>
	<style type="text/css">

		.acf-relationship .list {
			height: 400px;
		}

	</style>

	<?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');