<?php

/*

	Template Name: Immune Support Index

*/

get_header(); ?>

	<?php get_template_part('partials/hero'); ?>

	<?php get_template_part('partials/top-content'); ?>

	<section id="immune-system">
		<div class="wrapper">
			
			<div class="headline">
				<h3><?php the_field('immune_system_headline'); ?></h3>
			</div>

			<div class="photo">
				<img src="<?php $image = get_field('immune_system_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

		</div>
	</section>

	<?php get_template_part('partials/bottom-content'); ?>

	<?php get_template_part('partials/immune-support-nav'); ?>

<?php get_footer(); ?>