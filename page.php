<?php get_header(); ?>

	<section id="main" class="copy extended p2 generic">
		<div class="wrapper">

			<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

				<h1><?php the_title(); ?></h1>	
				<?php the_content(); ?>

			<?php endwhile; endif; ?>


		</div>
	</section>

<?php get_footer(); ?>