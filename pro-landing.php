<?php /* Template Name: Pro Landing */ ?>

<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_directory') ?>/css/Del-Immune.css" />

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?> id="top">

	<header>

		<section class="site-header pro">
			<div class="wrapper">

				<?php get_template_part('partials/header/logo'); ?>

				<?php get_template_part('partials/header/cart-link'); ?>

				<?php get_template_part('partials/header/desktop-utility-nav'); ?>

			</div>
		</section>

	</header>

	<section class="page-header pro">
		<div class="wrapper">

			<div class="page-title headline">
				<h1><?php the_field('pro_products_headline'); ?></h1>
			</div>

		</div>
	</section>

	<section class="products pro">
		<div class="wrapper">

			<section class="product-grid">
				<div class="grid-header headline">
					<h1>Pro Products</h1>
				</div>
				<?php echo do_shortcode('[bigcommerce_product per_page="12"]'); ?>
			</section>

		</div>
	</section>


	<footer class="site-footer pro">
		<div class="wrapper">

			<div class="newsletter">
				<div class="headline">
					<h4><?php the_field('newsletter_headline', 'options'); ?></h4>
				</div>

				<div class="copy p2">
					<p><?php the_field('newsletter_copy', 'options'); ?></p>
				</div>

				<div class="newsletter-form">
					<form>
						<div class="checkboxes">
							<h5>I'm interested in:</h5>
							<div class="checkbox">
								<input type="checkbox" id="immune-support" value="Immune Support" checked />
								<label for="immune-support">Immune Support</label>
							</div>

							<div class="checkbox">
								<input type="checkbox" id="pet-immune-support" value="Pet Immune Support" checked />
								<label for="pet-immune-support">Pet Immune Support</label>

							</div>
						</div>

						<div class="email">
							<label>Your email address</label>
							<input type="email" placeholder="name@email.com">
						</div>

						<div class="submit">
							<button type="submit" class="btn white">Sign Up</button>
						</div>

					</form>
				</div>
			</div>

			<div class="contact-info">
				<div class="headline">
					<h4><a href="<?php echo site_url('/contact/'); ?>"><?php the_field('contact_headline', 'options'); ?></a></h4>
				</div>

				<div class="copy p2">
					<p><?php the_field('address', 'options'); ?></p>
				</div>

				<div class="social">
					<?php if(have_rows('social_links', 'options')): while(have_rows('social_links', 'options')): the_row(); ?>
					 
					    <a href="<?php the_sub_field('link'); ?>">
					    	<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					    </a>

					<?php endwhile; endif; ?>
				</div>

				<div class="badges">
					<?php if(have_rows('badges', 'options')): while(have_rows('badges', 'options')): the_row(); ?>

						<?php if(get_sub_field('link')): ?>

							<div class="badge">
							    <a href="<?php the_sub_field('link'); ?>">
							    	<img src="<?php $image = get_sub_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							    </a>
							</div>
					
					    <?php else: ?>

							<div class="badge">
							    <img src="<?php $image = get_sub_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>
					
					    <?php endif; ?>

					<?php endwhile; endif; ?>
				</div>
				
			</div>

			<div class="copyright">
				<div class="copy p3">
					<?php the_field('copyright', 'options'); ?>
				</div>				
			</div>

		</div>
	</footer>

	<div id="back-to-top">
		<a href="#top" class="smooth">
			<div class="icon">
				<img src="<?php bloginfo('template_directory') ?>/images/back-to-top-arrow.svg" alt="" />
				
			</div>
			<div class="label">
				<span>Back to top</span>
			</div>
		</a>
	</div>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>

	<?php get_template_part('partials/footer/ga'); ?>
	
	<?php wp_footer(); ?>

</body>
</html>