	<footer class="site-footer">
		<div class="wrapper">

			<div class="newsletter">

				<div class="newsletter-form">
					<!-- Begin Constant Contact Inline Form Code -->
					<div class="ctct-inline-form" data-form-id="c1fb7872-1aa7-4ec7-83a1-afa6944fd847"></div>
					<!-- End Constant Contact Inline Form Code -->

					<!-- Begin Constant Contact Active Forms -->
					<script> var _ctct_m = "ac4ad2664422d7907623adfe3e557e0d"; </script>
					<script id="signupScript" src="//static.ctctcdn.com/js/signup-form-widget/current/signup-form-widget.min.js" async defer></script>
					<!-- End Constant Contact Active Forms -->
				</div>
			</div>

			<div class="contact-info">
				<div class="headline">
					<h4><a href="<?php echo site_url('/contact/'); ?>"><?php the_field('contact_headline', 'options'); ?></a></h4>
				</div>

				<div class="copy p2">
					<p><?php the_field('address', 'options'); ?></p>
				</div>

				<div class="social">
					<?php if(have_rows('social_links', 'options')): while(have_rows('social_links', 'options')): the_row(); ?>
					 
					    <a href="<?php the_sub_field('link'); ?>" rel="external">
					    	<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					    </a>

					<?php endwhile; endif; ?>
				</div>

				<div class="badges">
					<?php if(have_rows('badges', 'options')): while(have_rows('badges', 'options')): the_row(); ?>

						<?php if(get_sub_field('link')): ?>

							<div class="badge">
							    <a href="<?php the_sub_field('link'); ?>">
							    	<img src="<?php $image = get_sub_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							    </a>
							</div>
					
					    <?php else: ?>

							<div class="badge">
							    <img src="<?php $image = get_sub_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>
					
					    <?php endif; ?>

					<?php endwhile; endif; ?>
				</div>
				
			</div>

			<div class="copyright">
				<div class="copy p3">
					<?php the_field('copyright', 'options'); ?>
				</div>				
			</div>

		</div>
	</footer>

	<div id="back-to-top">
		<a href="#top" class="smooth">
			<div class="icon">
				<img src="<?php bloginfo('template_directory') ?>/images/back-to-top-arrow.svg" alt="" />
				
			</div>
			<div class="label">
				<span>Back to top</span>
			</div>
		</a>
	</div>

	<?php get_template_part('partials/footer/ga'); ?>
	
	<?php wp_footer(); ?>

</body>
</html>