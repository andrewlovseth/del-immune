<?php

/*

	Template Name: Our Story

*/

get_header(); ?>

	<?php get_template_part('partials/hero'); ?>

	<?php get_template_part('partials/top-content'); ?>

	<?php get_template_part('partials/middle-content'); ?>

	<?php get_template_part('partials/bottom-content'); ?>

	<?php get_template_part('partials/immune-support-nav'); ?>

<?php get_footer(); ?>