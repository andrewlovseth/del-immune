(function ($, window, document, undefined) {

	$(document).ready(function() {

		// rel="external"
		$('a[rel="external"]').click( function() {
			window.open( $(this).attr('href') );
			return false;
		});


		// Mobile Menu Toggle
		$('.menu-btn').click(function(){
			$(this).toggleClass('active');
			$('body').toggleClass('mobile-nav-open');


			return false;
		});


		// Mobile Nav Click Slide
		$('nav.mobile .main-screen a.subnav').on('click', function(){

			var link = $(this),
				subnav = $(link).data('subnav'),
				slider = $('nav.mobile .slide-wrapper');

			$(slider).addClass('subnav-active');

			$('#' + subnav).addClass('show');

			return false;
		});


		// Mobile Nav Click Back
		$('nav.mobile .subnav-screen .back a').on('click', function(){

			$('nav.mobile .subnav-screen .subnav').removeClass('show');
			$('nav.mobile .slide-wrapper').removeClass('subnav-active');

			return false;
		});



		// Desktop Nav Hover Dropdown
		$('nav.desktop a.dropdown').on('mouseover', function(){

			var link = $(this),
				dropdown = $(link).data('dropdown');

			if(!$(this).hasClass('active')) {
				removeActiveClass();
			}

			removeActiveClass();

			$('#' + dropdown + ', .dropdown-wrapper').addClass('active');
			$(link).addClass('active-link');
		});

		// Hide Dropdown when mouse leaves <header>
		$('header').on('mouseleave', function(){
			removeActiveClass();
		});


		// Hide Dropdown when hover other header links
		$('section.site-header a:not(.dropdown)').on('mouseover', function(){
			removeActiveClass();
		});


		// remove immune support nav items from same page
		$('.immune-support-nav .item-active').remove();


		// Home Hero Slider
		$('body.home section.hero-slider').slick({
			arrows: true,
			dots: true,
			adaptiveHeight: true,
			autoplay: true,
			autoplaySpeed: 5000,
		});


		// Home Quote Slider
		$('.quote-wrapper').slick({
			arrows: true,
			dots: true,
			adaptiveHeight: true,
			autoplay: true,
			autoplaySpeed: 5000
		});


		// Home Hero Nav Offset
		var heroImageHeight = $('body.home section.hero-slider .content').outerHeight();

		$('.hero-slider .slick-dots, .hero-slider .slick-prev, .hero-slider .slick-next').css('top', heroImageHeight);


		$('.faqs .question a').on('click', function(){


			var answerTarget = $(this).closest('.question').siblings('.answer');

			if (!$(this).hasClass('active')) {
				$('.faqs .question a').removeClass('active');
				$('.faqs .answer.active').slideUp(200);

				$(this).addClass('active')
				$(answerTarget).addClass('active').slideDown(200);
			} else {
				$(this).removeClass('active')
				$('.faqs .answer.active').slideUp(200);
			}

			return false;
		});


		// Immune Support Details Resources
		$('body.page-template-immune-support-detail .resources-wrapper').slick({
			arrows: true,
			dots: true,
			adaptiveHeight: true

		});


		// FAQ Smooth Links
		$('.section-links a').smoothScroll();

		// Smooth Links
		$('a.smooth').smoothScroll();

	});

	var wrap = $("body");

	wrap.on("scroll", function(e) {

	  if (this.scrollTop() > 147) {
		wrap.addClass("fix-search");
	  } else {
		wrap.removeClass("fix-search");
	  }

	});



	$(window).scroll(function() {    
		var scroll = $(window).scrollTop();
		var fold = $(window).height() + 100;

		if (scroll > fold) {
			wrap.addClass('past-fold');
		} else {
			wrap.removeClass('past-fold');
		}
	}); 



	// Remove class helper function
	function removeActiveClass() {
		$('section.dropdown, .dropdown-wrapper').removeClass('active');
		$('nav.desktop a.dropdown').removeClass('active-link');
	}


	// Hide Dropdown when clicked outside of nav
	$(document).mouseup(function(e)  {
		var container = $('nav.desktop');

		if (!container.is(e.target) && container.has(e.target).length === 0) {
			removeActiveClass();
		}
	});


	// Hide Dropdown on ESC
	$(document).on('keydown', function(e) {
		if ( e.keyCode === 27 ) {
			removeActiveClass();
		}
	});

	var resizeTimer;
	// Update Hero Nav Offset on Resize
	$(window).on('resize', function(e) {

		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {

			var heroImageHeight = $('body.home section.hero-slider .content').outerHeight();

			$('body.home section.hero-slider .slick-dots, body.home section.hero-slider .slick-prev, body.home section.hero-slider .slick-next').css('top', heroImageHeight);

		}, 250);

	});

})(jQuery, window, document);