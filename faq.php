<?php

/*

	Template Name: FAQ

*/

get_header(); ?>

	<section class="page-header">
		<div class="wrapper">

			<?php get_template_part('partials/breadcrumbs/blog-single'); ?>

			<div class="page-title headline">
				<h1><?php the_title(); ?></h1>
			</div>

			<div class="copy p2">
				<?php the_field('header_copy'); ?>
			</div>

			<div class="section-links">
				<?php if(have_rows('faq')): while(have_rows('faq')) : the_row(); ?>				 
				    <?php if( get_row_layout() == 'category' ): ?>

				    	<div class="section-anchor">
				    		<a href="#<?php echo sanitize_title_with_dashes(get_sub_field('headline'));?>"><?php the_sub_field('headline'); ?></a>
				    	</div>

					<?php endif; ?>
				<?php endwhile; endif; ?>
			</div>
			
		</div>
	</section>


	<?php if(have_rows('faq')): while(have_rows('faq')) : the_row(); ?>
	 
	    <?php if( get_row_layout() == 'category' ): ?>
			
			<section class="category" id="<?php echo sanitize_title_with_dashes(get_sub_field('headline'));?>">
				<div class="wrapper">

					<div class="headline">
						<h4><?php the_sub_field('headline'); ?></h4>
					</div>

					<div class="faqs">
										
						<?php if(have_rows('questions')): while(have_rows('questions')): the_row(); ?>

							<div class="question-wrapper">
							 
							    <div class="question">
							       <h3><a href="#"><span><?php the_sub_field('question'); ?></span></a></h3>
							    </div>

							    <div class="answer copy p2">
							    	<?php the_sub_field('answer'); ?>
							    </div>
		
							</div>

						<?php endwhile; endif; ?>

					</div>
					
				</div>
			</section>
			
	    <?php endif; ?>
	 
	<?php endwhile; endif; ?>

<?php get_footer(); ?>