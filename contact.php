<?php

/*

	Template Name: Contact

*/

get_header(); ?>

	<section class="main">
		<div class="wrapper">

			<section class="page-header">
				<?php get_template_part('partials/breadcrumbs/level-two-page'); ?>

				<div class="page-title headline">
					<h1><?php the_title(); ?></h1>
				</div>

				<div class="copy p2">
					<?php the_field('header_copy'); ?>
				</div>

				<section class="contact-form">
					<?php 
						$shortcode = get_field('contact_form_shortcode');
						echo do_shortcode($shortcode);
					?>				
				</section>

			</section>

			<section class="contact-info">
				<div class="phone contact-type">
					<p><?php the_field('phone_copy'); ?></p>
				</div>

				<div class="address contact-type">
					<h3><?php the_field('address_headline'); ?></h3>
					<p><?php the_field('address_copy'); ?></p>
				</div>
			</section>

		</div>		
	</section>

<?php get_footer(); ?>