<?php

/*

	Template Name: Immune Support Detail

*/

get_header(); ?>

	<?php get_template_part('partials/hero'); ?>

	<?php get_template_part('partials/top-content'); ?>

	
	<?php if(have_rows('heath_problems')): ?>
		<section id="health-problems" class="cover">
			<div class="wrapper">

				<div class="health-problems-wrapper">
					<div class="info">

						<div class="headline">
							<h1><?php the_field('health_problems_headline'); ?></h1>
						</div>
						
						<div class="problems">
							<?php while(have_rows('heath_problems')): the_row(); ?>
							 
							    <div class="problem">
							        <p><?php the_sub_field('problem'); ?></p>
							    </div>

							<?php endwhile; ?>
						</div>

					</div>
				</div>

			</div>
		</section>
	<?php endif; ?>


	<?php if(have_rows('quotes')): ?>

		<section id="quote-slider">
			<div class="wrapper">

				<div class="quote-wrapper">
					<?php while(have_rows('quotes')): the_row(); ?>

						<div class="quote">
							<div class="copy">
								<blockquote>
									<?php the_sub_field('quote'); ?>
								</blockquote>
							</div>

							<div class="source">
								<div class="photo">
									<img src="<?php $image = get_sub_field('source_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
								</div>
								<div class="details">
									<div class="content">
										<h4><?php the_sub_field('source_name'); ?></h4>
										<h5><?php the_sub_field('source_location'); ?></h5>
									</div>
								</div>
							</div>

						</div>

					<?php endwhile; ?>
				</div>
				
			</div>
		</section>
	
	<?php endif; ?>
	
	<?php $posts = get_field('resources'); if( $posts ): ?>

		<section class="resources">
			<div class="wrapper">

				<div class="section-header">
					<h2><?php the_field('resources_headline'); ?></h2>
				</div>

				<div class="resources-wrapper">
					
					<?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>

						<article>
							<?php if(get_field('featured_image', $p->ID)): ?>
								<div class="photo">
									<a href="<?php echo get_permalink( $p->ID ); ?>">
										<img src="<?php $image = get_field('featured_image', $p->ID); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									</a>
								</div>
							<?php endif; ?>

							<div class="info">
								<div class="headline">
									<h2><a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_title( $p->ID ); ?></a></h2>
								</div>

								<div class="copy p2">
									<?php echo wp_trim_words( get_post_field('post_content', $p->ID), 30, '...' ); ?>

								</div>

								<div class="cta">
									<a href="<?php echo get_permalink( $p->ID ); ?>" class="btn blue">Read More</a>
								</div>
							</div>							
						</article>

					<?php endforeach; ?>
			
				</div>			

			</div>
		</section>

	<?php endif; ?>		

	<section class="products">
		<div class="wrapper">

			<section class="product-grid">
				<div class="grid-header headline">
					<h1><?php the_field('products_headline'); ?></h1>
				</div>

				<?php 
					$product_ids = get_field('product_ids');
					echo do_shortcode('[bigcommerce_product per_page="3" id="' . $product_ids . '"]');
				?>
			</section>

		</div>
	</section>


	<?php get_template_part('partials/immune-support-nav'); ?>

<?php get_footer(); ?>