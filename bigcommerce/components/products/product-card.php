<?php
/**
 * Product Card used in loops and grids.
 *
 * @package BigCommerce
 *
 * @var Product $product
 * @var string  $title
 * @var string  $rating
 * @var string  $brand
 * @var string  $image
 * @var string  $price
 * @var string  $quick_view
 * @var string  $attributes
 */

use BigCommerce\Post_Types\Product\Product;

?>
<div class="bc-product__meta">
	<?php
	echo $title;
	echo $price;
	?>
</div>

<!-- data-js="bc-product-quick-view-dialog-trigger" is required -->
<button type="button" class="bc-quickview-trigger"
        data-js="bc-product-quick-view-dialog-trigger"
        data-content=""
        data-productid="<?php echo $product->post_id(); ?>"
        <?php echo $attributes;?>
>
	<?php echo $image; ?>
	<?php if ( $quick_view ) { ?>
	<div class="bc-quickview-trigger--hover">
		<span class="bc-quickview-trigger--hover-label">
			<?php echo esc_html( __( 'Quick View', 'bigcommerce' ) ); ?>
		</span>
	</div>
	<?php } ?>
</button>
<?php if ( $quick_view ) { ?>
<!-- data-js="" is required -->
<script data-js="" type="text/template">
	<!-- data-js="bc-product-quick-view-content" is required -->
	<section class="bc-product-quick-view__content-inner" data-js="bc-product-quick-view-content">
		<?php echo $quick_view; ?>
	</section>
</script>
<?php } ?>


<div class="bc-product-reviews">

</div>

<?php if ( ! empty( $form ) ) { ?>
	<!-- data-js="bc-product-group-actions" is required -->
	<div class="bc-product__actions" data-js="bc-product-group-actions">
		<?php echo $form; ?>
	</div>
<?php } ?>

<div class="bc-product-details">
	<a href="<?php echo get_permalink($product->post_id()); ?>">Product Details</a>
</div>
