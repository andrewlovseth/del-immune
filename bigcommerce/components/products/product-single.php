<?php
/**
 * @var string $images
 * @var string $title
 * @var string $brand
 * @var string $price
 * @var string $rating
 * @var string $form
 * @var string $description
 * @var string $sku
 * @var string $specs
 * @var string $related
 * @var string $reviews
 */
?>

<!-- data-js="bc-product-data-wrapper" is required. -->

<section class="page-header">
	<?php get_template_part('partials/breadcrumbs/products'); ?>	
</section>

<section class="bc-product-single__top" data-js="bc-product-data-wrapper">
	<?php echo $images; ?>

	<!-- data-js="bc-product-meta" is required. -->
	<div class="bc-product-single__meta" data-js="bc-product-meta">
		<?php echo $title; ?>
		<?php echo $brand; ?>
		<?php echo $price; ?>
		<?php echo $rating; ?>
		<?php echo $form; ?>

		<section class="bc-single-product__description">
			<?php echo $description; ?>
		</section>

	</div>
</section>

<section class="quote">
	<?php the_field('quote'); ?>
</section>

<?php echo $related; ?>

<?php echo $reviews; ?>