<?php
/**
 * The template for rendering the product archive page content
 *
 * @var string[] $posts
 * @var string   $no_results
 * @var string   $title
 * @var string   $description
 * @var string   $refinery
 * @var string   $pagination
 * @var string   $columns
 */

 /* Note: Deleted everything in this file since this site is using a custom category page */
?>

<div class="wrapper">
	<div class="content">
		<section class="page-header">
			<div class="page-title headline">
				<h1>Looking for Products?</h1>
			</div>
		</section>

		<div class="copy p2 extended">
			<p>View our product catalog <a href="/shop-products/">here</a>.</p>
		</div>
	</div>
</div>