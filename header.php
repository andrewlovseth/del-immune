<!DOCTYPE html>
<html>
<head>

	<?php the_field('head_js', 'options'); ?>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_directory') ?>/css/Del-Immune.css" />

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?> id="top">
	<?php the_field('body_js', 'options'); ?>

	<header>

		<section class="site-header">
			<div class="wrapper">

				<?php get_template_part('partials/header/logo'); ?>

				<?php get_template_part('partials/header/desktop-nav'); ?>

				<?php get_template_part('partials/header/cart-link'); ?>

				<?php get_template_part('partials/header/hamburger'); ?>

				<?php get_template_part('partials/header/desktop-utility-nav'); ?>

			</div>
		</section>

		<?php get_template_part('partials/header/mobile-nav'); ?>

		<?php get_template_part('partials/header/desktop-dropdowns'); ?>
		

	</header>
