<?php get_header(); ?>

	<?php
		$category = get_queried_object();
		$cat = $category->term_id;
		$cat_slug = $category->slug;
	?>

	<section class="page-header">
		<div class="wrapper">

			<?php get_template_part('partials/breadcrumbs/blog-category'); ?>

			<div class="page-header-wrapper">
				<div class="page-title headline">
					<h1>Resources</h1>
					<?php the_field('resources_deck', 'options'); ?>
				</div>
	
				<?php get_template_part('partials/category-dropdown'); ?>
					
			</div>
			
		</div>
	</section>



	<section class="category-header">
		<div class="wrapper">
			<h2><?php single_cat_title(); ?></h2>
		</div>
	</section>

	<section class="posts featured-posts">
		<div class="wrapper">
		
			<div class="featured-post">
				
				<?php
					$term = get_queried_object();
					$summary = get_field('featured_post_summary', $term);
					$post_object = get_field('featured_post', $term); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
					<article class="featured">
						<div class="photo">
							<a href="<?php the_permalink(); ?>">
								<img src="<?php $image = get_field('featured_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</a>
						</div>

						<div class="info">
							<div class="headline">
								<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
							</div>

							<div class="copy p2">
								<?php echo $summary; ?>
							</div>

							<div class="cta">
								<a href="<?php the_permalink(); ?>" class="btn blue">Read More</a>
							</div>
						</div>
						
					</article>
				<?php wp_reset_postdata(); endif; ?>


			</div>	

			<div class="popular-posts">
				<div class="headline section-headline">
					<h4>Recent Posts</h4>
				</div>

				<?php
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 5,
						'cat' => $cat
					);
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>


					<article class="popular">
						<div class="photo">
							<a href="<?php the_permalink(); ?>">
								<img src="<?php $image = get_field('featured_image'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
							</a>
						</div>

						<div class="info">
							<div class="headline">
								<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							</div>

							<div class="cta">
								<a href="<?php the_permalink(); ?>" class="text">Read More</a>
							</div>
						</div>
					</article>

				<?php endwhile; endif; wp_reset_postdata(); ?>					
			</div>

		</div>
	</section>



	<section class="posts all-posts">
		<div class="wrapper">

			<div class="headline section-headline">
				<h4>All Posts</h4>
			</div>

			<div class="post-wrapper">
				<?php echo do_shortcode('[ajax_load_more container_type="div" post_type="post" posts_per_page="8" scroll="false" button_label="Load More" category="' . $cat_slug . '"]'); ?>
			</div>

		</div>
	</section>

	
<?php get_footer(); ?>