<?php

/*

	Template Name: Product Category

*/

global $post;
$page_slug = $post->post_name;


get_header(); ?>

	<section class="page-header">
		<div class="wrapper">

			<?php get_template_part('partials/breadcrumbs/products'); ?>

			<div class="page-title headline">
				<h1>Shop Products</h1>
			</div>

			<div class="sort-by">
				
			</div>

		</div>
	</section>

	<section class="products">
		<div class="wrapper">

			<section class="sidebar <?php echo $page_slug; ?>">

				<div class="categories">
					<div class="category">
						<h4 class="all"><a href="<?php echo site_url('/shop-products/'); ?>">All Products</a></h4>
					</div>

					<?php if(have_rows('shop_products_dropdown', 'options')): while(have_rows('shop_products_dropdown', 'options')): the_row(); ?>
						<?php 
							$post_id = get_sub_field('page', false, false);
							$post_slug = get_post_field( 'post_name', $post_id );

							if( $post_id ): ?>

							<div class="category">
								<h4 class="<?php echo $post_slug; ?>"><a href="<?php echo get_the_permalink($post_id); ?>"><?php echo get_the_title($post_id); ?></a></h4>
							</div>

						<?php endif; ?>
					<?php endwhile; endif; ?>	
				</div>

			</section>

			<section class="product-grid">
				<div class="grid-header headline">
					<h1><?php the_title(); ?></h1>
				</div>

				<?php
					$category_slug = get_field('product_category');
					echo do_shortcode('[bigcommerce_product per_page="12" category="' . $category_slug . '"]');
				?>
			</section>

		</div>
	</section>

<?php get_footer(); ?>