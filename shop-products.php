<?php

/*

	Template Name: Shop Products

*/

get_header(); ?>

	<section class="page-header">
		<div class="wrapper">

			<?php get_template_part('partials/breadcrumbs/level-two-page'); ?>

			<div class="page-title headline">
				<h1>Shop Products</h1>
			</div>

			<div class="sort-by">
				
			</div>

		</div>
	</section>

	<section class="products">
		<div class="wrapper">

			<section class="sidebar all">

				<div class="categories">
					<div class="category">
						<h4 class="all"><a href="<?php echo site_url('/shop-products/'); ?>">All Products</a></h4>
					</div>

					<?php if(have_rows('shop_products_dropdown', 'options')): while(have_rows('shop_products_dropdown', 'options')): the_row(); ?>
						<?php $post_id = get_sub_field('page', false, false); if( $post_id ): ?>

							<div class="category">
								<h4><a href="<?php echo get_the_permalink($post_id); ?>"><?php the_sub_field('label'); ?></a></h4>
							</div>

						<?php endif; ?>
					<?php endwhile; endif; ?>		

					<?php if(have_rows('shop_products_extra_categories', 'options')): while(have_rows('shop_products_extra_categories', 'options')): the_row(); ?>
						<?php $post_id = get_sub_field('page', false, false); if( $post_id ): ?>

							<div class="category">
								<h4><a href="<?php echo get_the_permalink($post_id); ?>"><?php the_sub_field('label'); ?></a></h4>
							</div>

						<?php endif; ?>
					<?php endwhile; endif; ?>	

				</div>

			</section>

			<section class="product-grid">
				<div class="grid-header headline">
					<h1>All Products</h1>
				</div>
				<?php echo do_shortcode('[bigcommerce_product per_page="12"]'); ?>
			</section>

		</div>
	</section>

<?php get_footer(); ?>