<?php get_header(); ?>

	<section class="page-header">
		<div class="wrapper">

			<?php get_template_part('partials/breadcrumbs/search-results'); ?>

			<div class="page-header-wrapper">
				<div class="page-title headline">
					<h1>Search Results: <?php the_search_query(); ?></h1>

					<?php
         				global $wp_query; 
        				$found = $wp_query->found_posts;
					?>

					<h2><?php echo $found; ?> results found.</h2>
				</div>
			</div>
			
		</div>
	</section>


	<section class="posts all-posts">
		<div class="wrapper">

			<div class="post-wrapper">
				<?php
					$search_query = get_search_query();
					echo do_shortcode('[ajax_load_more id="searchwp" container_type="div" post_type="any" posts_per_page="10" search="' . $search_query . '" scroll="false" button_label="Load More"]');
				?>
			</div>
	
		</div>
	</section>
	
<?php get_footer(); ?>