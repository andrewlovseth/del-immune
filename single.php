<?php get_header(); ?>


	<section class="page-header">
		<div class="wrapper">

			<?php get_template_part('partials/breadcrumbs/blog-single'); ?>

		</div>
	</section>
	
	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<article <?php post_class(); ?>>
			<div class="wrapper">

				<section class="article-header">
					<div class="headline">
						<h1><?php the_title(); ?></h1>
					</div>

					<?php get_template_part('partials/single/share'); ?>

					<?php if(get_field('featured_image')): ?>
						<div class="featured-image">
							<img src="<?php $image = get_field('featured_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>
					<?php endif; ?>
						
				</section>

				<section class="article-body">
					<div class="copy p2 extended">
						<?php the_content(); ?>
					</div>					
				</section>

			</div>		
		</article>

	<?php endwhile; endif; ?>				

	<section class="similar-posts posts all-posts">
		<div class="wrapper">

			<div class="headline section-headline">
				<h4>Similar Posts</h4>
			</div>
			
			<div class="post-wrapper">

				<?php
					$post_categories = wp_get_post_categories($post->ID);
					$cats = array();
					$exclude = array();
					     
					foreach($post_categories as $c){
					    $cat = get_category( $c );
					    $cats[] = $cat->term_id;
					}

					$exclude[] = $post->ID;

					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 3,
						'category__in' => $cats,
						'orderby' => 'rand',
						'post__not_in' => $exclude

					);
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

					<article class="list">
						<div class="info">
							<div class="headline">
								<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							</div>

							<div class="cta">
								<a href="<?php the_permalink(); ?>" class="text">Read More</a>
							</div>
						</div>
					</article>

				<?php endwhile; endif; wp_reset_postdata(); ?>

			</div>
	
		</div>
	</section>

<?php get_footer(); ?>